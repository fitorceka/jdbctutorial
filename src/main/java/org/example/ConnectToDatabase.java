package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectToDatabase {
    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {

        try {
            // establish connection
            System.out.println("Connecting to database.....");
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected to database successfully!");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
