package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDatabase {
    static final String DB_URL = "jdbc:mysql://localhost";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {

        try {
            // establish connection

            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);

            System.out.println("Creating Database .......");
            String sql = "CREATE DATABASE STUDENTS";

            Statement statement = con.createStatement();

            statement.executeUpdate(sql);

            System.out.println("Database created successfully!");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
