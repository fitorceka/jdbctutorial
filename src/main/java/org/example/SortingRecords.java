package org.example;

import java.sql.*;

public class SortingRecords {

    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {
        try {
            // establish connection
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement statement = con.createStatement();

            String sql_desc = "Select * from general_info order by first_name desc";
            String sql_asc = "Select * from general_info order by first_name asc";

            ResultSet result = statement.executeQuery(sql_desc);

            while (result.next()) {
                System.out.print("ID: " + result.getInt("id"));
                System.out.print(", Age: " + result.getInt("age"));
                System.out.print(", First Name: " + result.getString("first_name"));
                System.out.println(", Last Name: " + result.getString("last_name"));
            }

            System.out.println("================================================");
            result = statement.executeQuery(sql_asc);

            while (result.next()) {
                System.out.print("ID: " + result.getInt("id"));
                System.out.print(", Age: " + result.getInt("age"));
                System.out.print(", First Name: " + result.getString("first_name"));
                System.out.println(", Last Name: " + result.getString("last_name"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
