package org.example;

import java.sql.*;

public class SelectPreparedStatement {
    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {
        try {
            // establish connection
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);

            String sql = "select * from general_info where id = ?";

            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "2");
            ResultSet result = preparedStatement.executeQuery();
            System.out.println("Using Prepared statement");
            while (result.next()) {
                System.out.print("ID: " + result.getInt("id"));
                System.out.print(", Age: " + result.getInt("age"));
                System.out.print(", First Name: " + result.getString("first_name"));
                System.out.println(", Last Name: " + result.getString("last_name"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
