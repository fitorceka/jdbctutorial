package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertRecords {
    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {
        try {
            // establish connection
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement statement = con.createStatement();

            System.out.println("Inserting records.....");

            String record1 = "INSERT INTO GENERAL_INFO VALUES (1, 'Ali', 'Mara', 27);";
            statement.executeUpdate(record1);

            String record2 = "INSERT INTO GENERAL_INFO VALUES (2, 'John', 'Wick', 50);";
            statement.executeUpdate(record2);

            String record3 = "INSERT INTO GENERAL_INFO VALUES (3, 'Dani', 'Brook', 35);";
            statement.executeUpdate(record3);

            String record4 = "INSERT INTO GENERAL_INFO VALUES (4, 'Mira', 'Smith', 21);";
            statement.executeUpdate(record4);

            String record5 = "INSERT INTO GENERAL_INFO VALUES (5, 'Alex', 'Smith', 19);";
            statement.executeUpdate(record5);

            System.out.println("Records inserted successfully!");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
