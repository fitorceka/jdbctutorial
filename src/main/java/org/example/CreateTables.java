package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTables {

    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {

        try {
            // establish connection
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement statement = con.createStatement();

            System.out.println("Creating tables .......");
            // table creation
            String sql = "CREATE TABLE GENERAL_INFO " +
                    "(ID INTEGER NOT NULL PRIMARY KEY, " +
                    "FIRST_NAME VARCHAR(100), " +
                    "LAST_NAME VARCHAR(100), " +
                    "AGE INTEGER);";

            statement.executeUpdate(sql);
            System.out.println("Table created successfully!");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
