package org.example;

import java.sql.*;

public class SelectRecords {
    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {
        try {
            // establish connection
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement statement = con.createStatement();

            System.out.println("Select All records");

            String sql = "select * from general_info;";
            ResultSet result = statement.executeQuery(sql);

            // print
            while (result.next()) {
                System.out.print("ID: " + result.getInt("id"));
                System.out.print(", Age: " + result.getInt("age"));
                System.out.print(", First Name: " + result.getString("first_name"));
                System.out.println(", Last Name: " + result.getString("last_name"));
            }

            System.out.println("==========================================");
            System.out.println("==========================================");

            PreparedStatement preparedStatement = con.prepareStatement("select * from general_info where id = ?");
            preparedStatement.setString(1, "2");
            ResultSet set = preparedStatement.executeQuery();
            System.out.println("Using Prepared statement");
            while (set.next()) {
                System.out.print("ID: " + result.getInt("id"));
                System.out.print(", Age: " + result.getInt("age"));
                System.out.print(", First Name: " + result.getString("first_name"));
                System.out.println(", Last Name: " + result.getString("last_name"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
