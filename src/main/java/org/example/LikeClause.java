package org.example;

import java.sql.*;

public class LikeClause {
    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {
        try {
            // establish connection
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement statement = con.createStatement();

            String sql = "select * from general_info " +
                    "where first_name like 'Al%'"; // start with Al

            ResultSet result = statement.executeQuery(sql);


            while (result.next()) {
                System.out.print("ID: " + result.getInt("id"));
                System.out.print(", Age: " + result.getInt("age"));
                System.out.print(", First Name: " + result.getString("first_name"));
                System.out.println(", Last Name: " + result.getString("last_name"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
