package org.example;

import java.sql.*;

public class UpdatingRecords {
    static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {
        try {
            // establish connection
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);

            String sql = "update general_info set age = ? where id = ?";
            String select = "select * from general_info";

            System.out.println("Updated record using Prepared statement");

            PreparedStatement preparedStatement = con.prepareStatement(sql);
            preparedStatement.setString(1, "77");
            preparedStatement.setString(2, "1");
            int rows = preparedStatement.executeUpdate();

            System.out.println("Rows affected: " + rows);

            Statement statement = con.createStatement();
            ResultSet result = statement.executeQuery(select);

            while (result.next()) {
                System.out.print("ID: " + result.getInt("id"));
                System.out.print(", Age: " + result.getInt("age"));
                System.out.print(", First Name: " + result.getString("first_name"));
                System.out.println(", Last Name: " + result.getString("last_name"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
