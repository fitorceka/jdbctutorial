package org.exercises;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class UpdateUsers {

    public static void main(String[] args) {
        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                PreparedStatement stmt = connection.prepareStatement("UPDATE users SET email = ? WHERE id = ?");
                Scanner scanner = new Scanner(System.in);
                System.out.println("Enter user ID:");
                int id = scanner.nextInt();
                System.out.println("Enter new email:");
                String newEmail = scanner.next();
                stmt.setString(1, newEmail);
                stmt.setInt(2, id);
                int rowsAffected = stmt.executeUpdate();
                System.out.println(rowsAffected + " row(s) updated");
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
