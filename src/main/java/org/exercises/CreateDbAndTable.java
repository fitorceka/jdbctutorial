package org.exercises;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDbAndTable {
    public static void main(String[] args) {
        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                Statement statement = connection.createStatement();
                String createDatabase = "create database if not exists users;";
                statement.executeUpdate(createDatabase);

                String createUsers = "create table if not exists users " +
                        "(id integer auto_increment not null primary key," +
                        "name varchar(255)," +
                        "email varchar(255)," +
                        "password varchar(255));";
                statement.executeUpdate(createUsers);

                String tableOrders = "create table if not exists orders " +
                        "(order_id int auto_increment not null primary key," +
                        "user_id int not null," +
                        "order_date date," +
                        "total_amount decimal(10,2)," +
                        "constraint fk_orders_users foreign key (user_id) references users(id));";
                statement.executeUpdate(tableOrders);
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
