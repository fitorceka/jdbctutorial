package org.exercises;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Batch {

    public static void main(String[] args) {
        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO users (name, email, password) VALUES (?, ?, ?)");
                Scanner scanner = new Scanner(System.in);
                System.out.println("How many users do you want to insert?");
                int numUsers = scanner.nextInt();
                for (int i = 0; i < numUsers; i++) {
                    System.out.println("Enter name for user " + (i + 1) + ":");
                    String name = scanner.next();
                    System.out.println("Enter email for user " + (i + 1) + ":");
                    String email = scanner.next();
                    System.out.println("Enter password for user " + (i + 1) + ":");
                    String password = scanner.next();
                    stmt.setString(1, name);
                    stmt.setString(2, email);
                    stmt.setString(3, password);
                    stmt.addBatch();
                }
                int[] rowsAffected = stmt.executeBatch();
                System.out.println(rowsAffected.length + " row(s) updated");
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
