package org.exercises;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertIntoTableUsers {
    public static void main(String[] args) {
        insert("John Doe", "johndoe@example.com", "password123");
        insert("John Smith", "johnsmith@example.com", "password1234");
        insert("John Wick", "johnwick@example.com", "password12345");
    }

    static void insert(String name, String email, String password) {
        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO users (name, email, password) VALUES (?, ?, ?)");
                stmt.setString(1, name);
                stmt.setString(2, email);
                stmt.setString(3, password);
                stmt.executeUpdate();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
