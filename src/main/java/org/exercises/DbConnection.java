package org.exercises;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
    static final String DB_URL = "jdbc:mysql://localhost:3306/users";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(DB_URL, USER, PASS);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
