package org.exercises;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateProcedure {
    public static void main(String[] args) {
        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                Statement statement = connection.createStatement();

                String procedure = "CREATE PROCEDURE get_orders_by_user (IN u_id INT)" +
                        "BEGIN" +
                        "  SELECT * FROM orders WHERE user_id = u_id;" +
                        "END;";
                statement.executeUpdate(procedure);
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
