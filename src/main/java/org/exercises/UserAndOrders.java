package org.exercises;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class UserAndOrders {
    public static void main(String[] args) {

        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT u.name, o.order_id, o.order_date, o.total_amount FROM users u JOIN orders o ON u.id = o.user_id");
                while (rs.next()) {
                    String name = rs.getString("name");
                    int orderId = rs.getInt("order_id");
                    Date orderDate = rs.getDate("order_date");
                    double totalAmount = rs.getDouble("total_amount");
                    System.out.println("Name: " + name + ", Order ID: " + orderId + ", Order Date: " + orderDate + ", Total Amount: " + totalAmount);
                }
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
