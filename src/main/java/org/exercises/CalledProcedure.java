package org.exercises;

import java.sql.*;
import java.util.Scanner;

public class CalledProcedure {
    public static void main(String[] args) {
        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                CallableStatement stmt = connection.prepareCall("{CALL get_orders_by_user(?)}");
                Scanner scanner = new Scanner(System.in);
                System.out.print("Enter user ID: ");
                int id = scanner.nextInt();
                stmt.setInt(1, id);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    int orderId = rs.getInt("order_id");
                    Date orderDate = rs.getDate("order_date");
                    double totalAmount = rs.getDouble("total_amount");
                    System.out.println("Order ID: " + orderId + ", Order Date: " + orderDate + ", Total Amount: " + totalAmount);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
