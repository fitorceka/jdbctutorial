package org.exercises;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertTableOrders {
    public static void main(String[] args) {
        insert("1", "2020-10-10", "10.2");
        insert("1", "2021-10-10", "23.3");
        insert("2", "2022-10-10", "182.4");
    }

    static void insert(String userId, String orderDate, String totalAmount) {
        try {
            Connection connection = DbConnection.getConnection();
            if (connection != null) {
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO orders (user_id, order_date, total_amount) VALUES (?, ?, ?)");
                stmt.setString(1, userId);
                stmt.setString(2, orderDate);
                stmt.setString(3, totalAmount);
                stmt.executeUpdate();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
