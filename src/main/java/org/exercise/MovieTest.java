package org.exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MovieTest {
    static final String DB_URL = "jdbc:mysql://localhost:3306/movies";
    static final String USER = "root";
    static final String PASS = "milan1899";

    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
            MovieDAO movieDAO = new MovieDAOImpl(connection);

            movieDAO.createTable();
            movieDAO.createMovie(new Movie("John Wick", "Action", 2014));
            movieDAO.createMovie(new Movie("Alien", "Fantasy", 1980));
            movieDAO.findMovieById(1).ifPresent(System.out::println);
            movieDAO.updateMoviesTitle(1, "MR John Wick");
            movieDAO.findAll().forEach(System.out::println);
            movieDAO.deleteMovie(2);
            System.out.println(movieDAO.findAll().size());
            movieDAO.deleteTable();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
