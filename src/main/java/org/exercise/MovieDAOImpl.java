package org.exercise;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class MovieDAOImpl implements MovieDAO {

    private Connection connection;

    public MovieDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void createTable() {
        try {
            Statement createTableStatement = connection.createStatement();
            final String createTableQuery = "CREATE TABLE IF NOT EXISTS MOVIES (id INTEGER AUTO_INCREMENT, " +
                    "title VARCHAR(255), " +
                    "genre VARCHAR(255), " +
                    "yearOfRelease INTEGER, " +
                    "PRIMARY KEY (id))";
            createTableStatement.execute(createTableQuery);
        } catch (SQLException exp) {
            exp.printStackTrace();
        }
    }

    @Override
    public void deleteTable() {
        try {
            Statement deleteStructureStatement = connection.createStatement();
            deleteStructureStatement.execute("DROP TABLE MOVIES");
        } catch (SQLException exp) {
            exp.printStackTrace();
        }
    }

    @Override
    public void createMovie(Movie movie) {
        try {
            String sql = "INSERT INTO MOVIES (title, genre, yearOfRelease) VALUES (?, ?, ?)";
            PreparedStatement insertItemStatement = connection.prepareStatement(sql);
            insertItemStatement.setString(1, movie.getTitle());
            insertItemStatement.setString(2, movie.getGenre());
            insertItemStatement.setInt(3, movie.getYearOfRelease());
            insertItemStatement.executeUpdate();
        } catch (SQLException exp) {
            exp.printStackTrace();
        }
    }

    @Override
    public void deleteMovie(int id) {
        try {
            PreparedStatement deleteItemStatement = connection.prepareStatement("DELETE FROM MOVIES WHERE id = ?");
            deleteItemStatement.setInt(1, id);
            deleteItemStatement.executeUpdate();
        } catch (final SQLException exp) {
            exp.printStackTrace();
        }
    }

    @Override
    public void updateMoviesTitle(int id, String newTitle) {
        try {
            PreparedStatement updateStatement = connection.prepareStatement("UPDATE MOVIES SET title = ? WHERE id = ?");
            updateStatement.setString(1, newTitle);
            updateStatement.setInt(2, id);
            updateStatement.executeUpdate();
        } catch (final SQLException exp) {
            exp.printStackTrace();
        }
    }

    @Override
    public Optional<Movie> findMovieById(int id) {
        try {
            PreparedStatement searchStatement = connection.prepareStatement("SELECT * FROM MOVIES WHERE id = ?");
            searchStatement.setInt(1, id);
            final boolean searchResult = searchStatement.execute();
            if (searchResult) {
                final ResultSet foundMovie = searchStatement.getResultSet();
                if (foundMovie.next()) {
                    final String title = foundMovie.getString(2);
                    final String genre = foundMovie.getString(3);
                    final Integer yearOfRelease = foundMovie.getInt(4);
                    return Optional.of(new Movie(id, title, genre, yearOfRelease));
                }
            }
            return Optional.empty();
        } catch (final SQLException exp) {
            exp.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public List<Movie> findAll() {
        try {
            Statement createTableStatement = connection.createStatement();
            String findAllQuery = "SELECT * FROM MOVIES";
            ResultSet moviesResultSet = createTableStatement.executeQuery(findAllQuery);
            List<Movie> movies = new ArrayList<>();
            while (moviesResultSet.next()) {
                Integer id = moviesResultSet.getInt(1);
                String title = moviesResultSet.getString(2);
                String genre = moviesResultSet.getString(3);
                Integer yearOfRelease = moviesResultSet.getInt(4);
                movies.add(new Movie(id, title, genre, yearOfRelease));
            }

            return movies;
        } catch (final SQLException exp) {
            exp.printStackTrace();
        }

        return Collections.emptyList();
    }
}
